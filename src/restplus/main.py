import micropydd
from flask import Flask
from micropydd.config import Config
from micropydd.module import init
from micropydd_restplus.config import RestplusConfig
from micropydd_restplus.module import MicroPyDDRestplusModule


class ProjectConfig(Config, RestplusConfig):
    REST_API_VERSION: str = '0.0.1'
    REST_API_DESCRIPTION: str = 'This is my hello world api'
    REST_API_NAME: str = 'API Hello'
    REST_ROOT = ''

init({
    'default': ProjectConfig
}, additional_modules=[MicroPyDDRestplusModule()])

app = micropydd.app_context[Flask]

if __name__ == '__main__':
    app.run()
