"""
Simple installation of the framework. Just initialization using default configuration
"""
import micropydd
from micropydd.config import Config
from micropydd.logger import LoggerService
from micropydd.module import init

init({
    'default': Config
})

logger_service = micropydd.app_context[LoggerService]
print(LoggerService.__name__)
